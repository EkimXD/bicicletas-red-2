var express = require('express');
var router = express.Router();
const Usuarios=require("../controllers/usuarios");

/* GET users listing. */
router.get('/', Usuarios.mostrarUsuarios);
router.get('/create', Usuarios.crearUsuarioGet);
router.post('/create', Usuarios.crearUsuario);
// router.get('/:id', Usuarios.buscarUsuario);
// router.post('/:id', Usuarios.buscarUsuario);

router.post('/delete/:id',Usuarios.eliminarUsuario);
router.get('/update/:id',Usuarios.actualizarUsuarioGet);
router.post('/update/:id',Usuarios.actualizarUsuario);

module.exports = router;
