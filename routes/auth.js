const express=require('express');
const router=express.Router();
const autentificacion=require('../controllers/auth')

router.get('/:token',autentificacion.autentificateAuth);

module.exports=router;