var express = require('express');
var router = express.Router();
var usuariosContoller = require('../../controllers/api/usuariosAPI')

router.post('/', usuariosContoller.crearUsuario); //obtener lista
router.post('/reserva', usuariosContoller.createReserva); //obtener lista

module.exports = router;