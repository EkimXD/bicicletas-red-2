require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var bicicletasRouter=require('./routes/bicicletas');
var bicicletasRouterAPI=require('./routes/api/bicicletasAPI');
var usuarioRouterAPI=require('./routes/api/usuarios');
var authRouter=require('./routes/auth');
var passport=require('./config/passport');
var session=require('express-session');


const store=new session.MemoryStore;

var app = express();

app.use(session({
  cookie:{maxAge:240*60*60*1000},
  store:store,
  saveUninitialized:true,
  resave:'true',
  secret:'lol XDXDXDXDXD'
}))
//importingo mongoose

const mongoose = require('mongoose');
var mongo=process.env.MONGO_URI;
mongoose.connect(mongo, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useCreateIndex', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('we are connected');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/login', (req,res,nest)=>{
  res.render('authentification/login')
});

app.post('/login', (req,res,next)=>{
  passport.authenticate('local',(err,user,info)=>{
    if(err) return next(err);
    if(!user) return res.redirect('/login');
    req.logIn(user,(err)=>{
      if(err)return next(err);
      return res.redirect('/');
    })
  })(req,res,next);
});

app.get('/logout', (req,res,next)=>{
  req.logOut();
  res.render('/');
});



app.use('/', indexRouter);
app.use('/usuarios', usersRouter);
app.use('/bicicletas',bicicletasRouter);
app.use('/api', bicicletasRouterAPI);
app.use('/api/v1/usuario', usuarioRouterAPI);
app.use('/auth', authRouter);

app.use('/politica', (req,res)=>{
  res.sendFile( __dirname+'/public/politica.html');
})

app.use('/googlefa78e2b00a50b9e2', (req,res)=>{
  res.sendFile( __dirname+'/public/googlefa78e2b00a50b9e2.html');
})
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req,res,next){
  if(req.user){
    next();
  }else{
    res.redirect('/login');
  }
}


module.exports = app;
