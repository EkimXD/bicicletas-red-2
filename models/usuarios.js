var mongoose = require('mongoose');
const Reserva=require('../models/reservas');
let Schema = mongoose.Schema;
const bcrypt=require('bcryptjs');
const mongooseValidator=require('mongoose-unique-validator');
const nodemailer = require('nodemailer');
const Token=require('./token')

const saltBcrypt=10;

var usuariosSchema = new Schema({
    nombre: {
        type:String,
        trim:true,
        required:[true,'Es necesario el nombre']
    },
    email:{
        type:String,
        trim:true,
        required:[true,'Es necesario el email'],
        lowercase:true,
        unique:true,
        validate:[validateEmail,'Por favor ingrese un email valido'],
        match:[/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/]
    },
    password:{
        type:String,
        required:[]
    },
    // passwordResetToken:String,
    // passwordResetTokenExpired:Date,
    verified:{
        type:Boolean,
        default:false
    }
});

usuariosSchema.plugin(mongooseValidator,{message:'El {PATH} ya existe con otro usuario'});

function validateEmail(email){
    let regex=/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    return regex.test(email);
}

usuariosSchema.statics.verifyPassword=(password)=>{
    return bcrypt.compareSync(password,this.password);
};

usuariosSchema.statics.reserva=function(id_bicicleta, desde, hasta,id_usuario=this._id){
    let reserva=new Reserva({
        usuario:id_usuario,
        bicicleta:id_bicicleta,
        desde:desde,
        hasta:hasta
    })
    return reserva.save();
}

usuariosSchema.statics={
    allUsers:function(){
        return this.find({});
    },
    createUser:function(usuario){
        usuario.password=bcrypt.hashSync(usuario.password,saltBcrypt);
        return this.create(usuario);
    },
    updateUser:function(id, usuario){
        return this.findOneAndUpdate({_id:id},usuario);
    },
    deleteUser:function(id){
        return this.deleteOne({_id:id});
    },
    findUser:function(id){
        return this.findById(id);
    }
}

usuariosSchema.statics.enviarEmail=(usuarioId,email)=>{
    let mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'magali71@ethereal.email',
            pass: 'aBwVcHS1dDAU6wqM5A'
        }
    };
    let transporter = nodemailer.createTransport(mailConfig);
    const tokenNumber=usuarioId;
    const message = {
        from: 'noreply@bicis.com',
        to: email,
        subject: 'Confirmacion de cuenta',
        text: 'Para activar su cuenta por favor acceda a esta direction http://'+'localhost:3000'+'/auth/'+tokenNumber,
        html: '<h1>Activacion de cuenta</h1><p>Para activar su cuenta por favor acceda a esta direction</p><p>http://'+'localhost:3000'+'/auth/'+tokenNumber
    };
    let token=new Token({
        _userId:usuarioId,
        token:tokenNumber
    }).save();
    transporter.sendMail(message);
}

module.exports = mongoose.model('Usuario', usuariosSchema);