var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    coordenada: {
        type: [Number],
        index: { 
            type: '2dsphere',
            sparce: true
        }
    }
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, coordenada) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        coordenada: coordenada
    });
};

bicicletaSchema.methods.toString = function () {
    return 'code:' + this.code + " | color: " + this.color+"| id: "+this.id;
};

bicicletaSchema.statics.allBicis = function () {    
    return this.find({})
};

bicicletaSchema.statics.add = function (aBici) {
    return this.create(aBici);
}

bicicletaSchema.statics.updateOne = function (id, bici) {
    return this.updateOne({_id:id},{$set:bici});
}

bicicletaSchema.statics.findByCode = function (aCode) {
    return this.findOne({
        code: aCode
    });
};

bicicletaSchema.statics.removeByCode = function (aCode) {
    return this.deleteOne({
        code: aCode
    });
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);