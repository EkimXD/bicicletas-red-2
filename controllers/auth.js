const Token=require('../models/token');
const Usuarios=require('../models/usuarios');

module.exports={
    autentificateAuth:(req,res)=>{
        Token.findToken(req.params.token)
        .then(
            result=>{
                return Usuarios.findUser(result._userId);
            }
        ).then(
            usuario=>{
                console.log(usuario);
                console.log('\n\n'+usuario.id)
                return Usuarios.updateUser(usuario.id,{verified:true});
            }
        ).then(
            result=>{
                console.log(result);
                res.redirect('/usuarios');
            }
        ).catch(
            err=>{
                console.log('token no encontrado o token no valido' +err);
                res.redirect('/usuarios');
            }
        )
    }
}