const Bicicleta=require ('../../models/bicicleta');

exports.bicicleta_show=function(req,res){
    Bicicleta.allBicis()
    .then(
        result=>{
            res.status(200).json({
                bicicletas:result,
            });
        }
    ).catch(
        error=>{
            console.error(error);
        }
    )
}

exports.bicicleta_create=function(req,res){
    const bici=Bicicleta.createInstance(
        req.body.id,
        req.body.color,
        req.body.modelo,
        [req.body.lat,req.body.long]);
    Bicicleta.add(bici)
    .then(
        result=>{
            res.status(200).json({
                bicicleta:bici,
            })
        }
    ).catch(
        error=>{
            console.log(error);
        }
    )    
}

exports.bicicleta_delete=function(req,res){
    Bicicleta.removeByCode(req.body.id)
    .then(
        result=>{
            res.status(200).json({
                bicicleta:"deleted "+req.body.id,
            })
        }
    ).catch(
        error=>{
            throw new Error('Error '+error);
        }
    );    
}


exports.bicicleta_update=function(req,res){
    const bici=Bicicleta.createInstance(
        req.body.id,
        req.body.color,
        req.body.modelo,
        [req.body.lat,req.body.long]);
    Bicicleta.updateOne(req.body.id,bici)
    .then(
        result=>{
            res.status(200).json({
                bicicleta:bici,
            })
        }
    )
    .catch(
        error=>{
            throw new Error('Error '+error);
        }
    );    
    
}