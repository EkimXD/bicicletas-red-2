var Usuarios = require('../models/usuarios');

module.exports={
    crearUsuario:(req,res)=>{
        const usuario=new Usuarios({
            nombre:req.body.nombre,
            email:req.body.email,
            password:req.body.password
        });
        Usuarios.createUser(usuario)
        .then(
            result=>{
                Usuarios.enviarEmail(result.id,result.email);
                res.redirect('/usuarios/')
            }
        ).catch(
            err=>{
                console.log(err);
            }
        )
    },
    crearUsuarioGet:(req,res)=>{
        res.render('usuario/create');
    },
    eliminarUsuario:(req,res)=>{
        const id=req.params.id;
        Usuarios.deleteUser(id)
        .then(
            result=>{
                res.redirect('/usuarios/');
            }
        ).catch(
            err=>{
                throw new Error(err);
            }
        )
    },
    actualizarUsuario:(req,res)=>{
        const usuario={
            nombre:req.body.nombre,
            email:req.body.email,
            password:req.body.password
        };
        const id=req.params.id;
        Usuarios.updateUser(id,usuario)
        .then(
            result=>{
                res.redirect('/usuarios');
            }
        ).catch(
            err=>{
                throw new Error(err);
            }
        )
    },
    actualizarUsuarioGet:(req,res)=>{
        Usuarios.findUser(req.params.id)
        .then(
            result=>{
                res.render('usuario/update',{usuario:result});
            }
        ).catch(
            err=>{
                throw new Error(err);
            }
        )
    },
    buscarUsuario:(req,res)=>{
        Usuarios.findUser(req.params.id)
        .then(
            result=>{
                res.status(200).json(result);
            }
        ).catch(
            err=>{
                console.log(err);
            }
        )
    },
    mostrarUsuarios:(req,res)=>{
        Usuarios.allUsers()
        .then(
            result=>{
                res.render('usuario/index', {usuarios:result});
            }
        ).catch(
            err=>{
            console.log(err);
            }
        )
    }
}