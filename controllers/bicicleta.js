const Bicicleta= require ('../models/bicicleta');

exports.bicicleta= function(req,res){
    Bicicleta.allBicis()
    .then(
        result=>{
            res.render('bicicleta/index',{bicis:result});
        }
    ).catch(
        error=>{
            console.error("Error: "+error);
        }
    );
}

exports.bicicletaCreateGet=function(req,res){
    res.render('bicicleta/create');
}

exports.bicicletaCreatePost= function(req,res){
    const bici=Bicicleta.createInstance(
        req.body.id,
        req.body.color,
        req.body.modelo,
        [req.body.lat,req.body.long]);
    Bicicleta.add(bici)
    .then(
        result=>{
            res.redirect('/bicicletas');
        }
    ).catch(
        error=>{
            console.log(error);
        }
    )
}

exports.bicicletaUpdateGet=function(req,res){

    Bicicleta.findByCode(req.params.id)
    .then(
        result=>{
            res.render('bicicleta/update',{bici:result});
        }
    ).catch(
        error=>{
            throw new Error('Error: '+error);
        }
    )
}

exports.bicicletaUpdatePost=function(req,res){

    const bici=Bicicleta.createInstance(
        req.body.id,
        req.body.color,
        req.body.modelo,
        [req.body.lat,req.body.long]);
    Bicicleta.updateOne(req.body.id,bici)
    .then(
        result=>{
            res.redirect('/bicicletas')
        }
    )
    .catch(
        error=>{
            throw new Error('Error '+error);
        }
    );    
}

exports.bicicletaDelete=function(req,res){
    Bicicleta.removeByCode(req.body.id)
    .then(
        result=>{
            res.redirect('/bicicletas');
        }
    ).catch(
        error=>{
            throw new Error('Error '+error);
        }
    );
}