var map = L.map('main_map').setView([-0.2298500, -78.5249500], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

console.log('paso');
fetch("/api")
    .then(
        response=>{
            return response.json();
        }
    ).then(
        response=>{
            response.bicicletas.forEach(element => {
                console.log(element.coordenada);
                L.marker(element.coordenada,{title:element.code}).addTo(map);
            });
        }
    ).catch(
        error=>{
            console.log(error);
        }
    )