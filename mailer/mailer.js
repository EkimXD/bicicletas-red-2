const nodemailer = require('nodemailer');
const { ConsoleReporter } = require('jasmine');
const sgTransport=require('nodemailer-sendgrid-transport');
const auth = require('../controllers/auth');


let mailConfig;

if(process.env.NODE_ENV=='production'){
    const options={
        auth:{
            api_key:process.env.SENDGRID_APIKEY
        }
    };
    mailConfig=sgTransport(options);
}else{
    if(process.env.NODE_ENV=='staying'){
        const options={
            auth:{
                api_key:process.env.SENDGRID_APIKEY
            }
        };
        mailConfig=sgTransport(options);
    }else{
        mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'magali71@ethereal.email',
            pass: 'aBwVcHS1dDAU6wqM5A'
        }
    };
    }
    
}


module.exports={
    enviarEmail:(usuarioId,email)=>{
        let transporter = nodemailer.createTransport(mailConfig);
        const tokenNumber=usuarioId;
        const message = {
            from: 'noreply@bicis.com',
            to: email,
            subject: 'Confirmacion de cuenta',
            text: 'Para activar su cuenta por favor acceda a esta direction http://'+'localhost:3000'+'/auth/'+tokenNumber,
            html: '<h1>Activacion de cuenta</h1><p>Para activar su cuenta por favor acceda a esta direction</p><p>http://'+'localhost:3000'+'/auth/'+tokenNumber
        };
        let token=new Token({
            _userId:usuarioId,
            token:tokenNumber
        }).save();
        transporter.sendMail(message);
    }
    
};