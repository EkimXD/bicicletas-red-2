const passport=require('passport');
const localStrategy=require('passport-local').Strategy;
const usuario=require('../models/usuarios');

passport.use(new localStrategy(
    (email,password,done)=>{
        usuario.findOne({email:email},(err,usuario)=>{
            if(err)return done(err);
            if(!usuario)return done(null,false,{message:'no existe un usuario'});
            if(!usuario.verifyPassword(password))return done(null, false,{message:'contrasena incorrecta'});

            return(null,user);
        })
    }
));

passport.serializeUser(function (user,cb){
    cb(null,user.id);
});

passport.deserializeUser(function(id,cb){
    usuario.findById(id,function(err,usuario){
        cb(err,usuario);
    })
});

module.exports=passport;